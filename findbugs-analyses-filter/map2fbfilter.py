import os

curdir = os.curdir
filterdir = curdir + "/../findbugs-analyses-filter"

commalist = ""

with open(filterdir + "/map.txt") as f:
	for line in f:
		if line.startswith("\t"):
			line = line.strip()
			commalist += "," + line

commalist = commalist.lstrip(",")

print("<FindBugsFilter>")
print("\t<Match>")
print("\t\t<Bug pattern=\"" + commalist + "\"/>")
print("\t</Match>")
print("</FindBugsFilter>")
