Helper scripts for running FindRealBugs on the Qualitas Corpus on a build
server.

These things are expected to be present on the build server:
	~/findrealbugs-helper-scripts
		Clone of the findrealbugs-helper-scripts Git repository.
	~/qualitas-corpus/QualitasCorpus-20130901r/Systems
		Extracted Qualitas Corpus.

Run
	crontab ~/findrealbugs-helper-scripts/crontab.txt
to register the cron job, or use 'crontab -e' to enter the line from crontab.txt
manually.

Test results are written to a
	~/corpus-results-<date>-<time>/
directory. This directory will contain one *.txt file for each Qualitas Corpus
system FindRealBugs was tested on, containing the console output of the
FindRealBugs' invocation. The directory will also contain a test-log.txt file
which contains the console output of the test script, i.e. progress/summary
information.
