#!/bin/bash

dir="$1"

find "$dir" -type f -name "*.txt" -print0 | while read -d $'\0' txt
do
	echo "$txt"
	./uncolor.sh < "$txt" > "${txt}.new"
	mv "${txt}.new" "$txt"
done
