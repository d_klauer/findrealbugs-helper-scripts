#!/bin/bash
set -e

# Prevent next cron job from starting the next day in case the previous one is
# still running. (there shouldn't be any races, assuming the cronjob is @daily)
lockfile="findrealbugs-cronjob-active.lock"
if [ -f "$lockfile" ]; then
    exit 1
fi
touch "$lockfile"

# Update FindRealBugs clone, run tests, rebuild the .jar
git clone https://bitbucket.org/d_klauer/opal.git
cd opal
~/findrealbugs-helper-scripts/run-sbt.sh clean copy-resources doc compile assembly
cd ..

findrealbugs-helper-scripts/test-with-findrealbugs.sh
#findrealbugs-helper-scripts/test-with-findbugs.sh

rm -rf opal/
rm "$lockfile"
