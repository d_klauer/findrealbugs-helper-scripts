#!/bin/sh
# Filter out ANSI color escape sequences,
# and also <CR> <ESC> M which is produced by sbt...
sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g" | sed -r "s/\r\x1BM//g"
