#!/bin/sh
set -e

systemsdir="qualitas-corpus/QualitasCorpus-20130901r/Systems"

# Argument 1 = command to test on the Qualitas Corpus systems
testcommand="$1"
if [ -z "$1" ]; then
    echo "missing test command as first argument"
    exit 1
fi

# Argument 2 = list of system names of systems to test (useful when testing a
# subset manually). Empty = test all.
systems="$2"
if [ -z "$systems" ]; then
    systems=`ls $systemsdir`
fi

# Argument 3 = results directory.
resultsdir="$3"
if [ -z "$resultsdir" ]; then
    resultsdir="corpus-results/`date '+%Y-%m-%d-%H-%M-%S'`"
fi
mkdir -p "$resultsdir"

echo "Testing with '$testcommand'. Writing results into '$resultsdir'..."

total=`echo $systems | wc -w`
# Strip leading whitespace added by wc
total=`echo $total | sed -e 's/^[ \t]*//'`

count="0"

run_on_system() {
    system="$1"
    if $testcommand "$systemsdir/$system" > "$resultsdir/$system.txt" 2>&1; then
        echo "ok"
    else
        echo "crashed/aborted"
    fi
}

for i in $systems; do
    echo "----------------------------------------"
    date '+%Y-%m-%d %H:%M:%S'
    count=`expr $count + 1`
    echo "($count/$total) Testing on $i..."
    { time run_on_system "$i"; } 2>&1
done
