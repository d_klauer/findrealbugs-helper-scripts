#!/bin/sh
set -e

resultsdir="corpus-results/`date '+%Y-%m-%d-%H-%M-%S'`"
findbugs="java -Xmx4g -jar findbugs-3.0.0/lib/findbugs.jar -textui -include findrealbugs-helper-scripts/findbugs-analyses-filter/fb-filter.xml -longBugCodes -low -effort:max -maxRank 20 -relaxed"

mkdir -p "$resultsdir/findbugs"
{
	echo "Testing with FindBugs..."
	echo "begin: `date`"
	findrealbugs-helper-scripts/corpus.sh "$findbugs" "" "$resultsdir/findbugs"
	echo "end: `date`"
} 2>&1 | tee "$resultsdir/findbugs/test-log.txt"
