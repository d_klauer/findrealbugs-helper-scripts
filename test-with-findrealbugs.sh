#!/bin/sh
set -e

resultsdir="corpus-results/`date '+%Y-%m-%d-%H-%M-%S'`"
findrealbugs="java -Xmx4g -jar opal/OPAL/frb/cli/target/scala-2.11/FindREALBugs-0.0.1-SNAPSHOT.jar --config=findrealbugs-helper-scripts/analyses.frb"

mkdir -p "$resultsdir/findrealbugs"
{
	echo "Testing with FindRealBugs..."
	echo "begin: `date`"
	findrealbugs-helper-scripts/corpus.sh "$findrealbugs" "" "$resultsdir/findrealbugs"
	echo "end: `date`"
} 2>&1 | tee "$resultsdir/findrealbugs/test-log.txt"
