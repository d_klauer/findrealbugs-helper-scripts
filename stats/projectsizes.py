import sys
import os
from collections import OrderedDict

class ProjectInfo:
	seconds = 0.0
	classes = 0
	fields = 0
	methods = 0
	codesize = 0
	total = 0

projects = OrderedDict()

def readTestLog(testlog):
	print("reading test-log.txt: " + testlog)
	with open(testlog) as f:
		while True:
			line = f.readline()
			if not line:
				break

			if line == "----------------------------------------\n":
				# Date
				line = f.readline()

				# (1/110) Testing on <projectname>...
				line = f.readline().rstrip("\n")
				assert "Testing on " in line
				projectname = line[line.find("Testing on ") + 11:len(line)-3]

				# ok or crashed/aborted
				line = f.readline().rstrip("\n")
				assert line == "ok" or line == "crashed/aborted"

				# Empty line
				line = f.readline().rstrip("\n")
				assert len(line) == 0

				# real\t0m5.492s
				line = f.readline().rstrip("\n")
				assert line.startswith("real\t")
				realtime = line[5:]

				# <minutes>m<seconds>s
				mpos = line.find("m")
				spos = line.find("s")
				minutes = line[5:mpos]
				seconds = line[mpos+1:spos]
				#print(realtime + ", " + minutes + ", " + seconds)

				totalseconds = "%0.3f" % (float(minutes) * 60 + float(seconds))
				#print(realtime + ", " + totalseconds)
				projects[projectname] = ProjectInfo()
				projects[projectname].seconds = totalseconds

def readProjectLog(project, projectlog):
	print("reading " + projectlog)
	with open(projectlog) as f:
		lines = f.readlines()
		lines = lines[-5:]

		assert lines[0].startswith("  classes: ")
		projects[project].classes = lines[0][11:].rstrip("\n")

		assert lines[1].startswith("   fields: ")
		projects[project].fields = lines[1][11:].rstrip("\n")

		assert lines[2].startswith("  methods: ")
		projects[project].methods = lines[2][11:].rstrip("\n")

		assert lines[3].startswith("code size: ")
		projects[project].codesize = lines[3][11:].rstrip("\n")

		assert lines[4].startswith("    total: ")
		projects[project].total = lines[4][11:].rstrip("\n")

def writeSizeMatrix(csvfile):
	print("writing project size matrix: " + csvfile)
	with open(csvfile, "w") as f:
		f.write(",classes,fields,methods,codesize,total\n")
		for project in projects:
			f.write(project                    + ",")
			f.write(projects[project].classes  + ",")
			f.write(projects[project].fields   + ",")
			f.write(projects[project].methods  + ",")
			f.write(projects[project].codesize + ",")
			f.write(projects[project].total    + "\n")

if len(sys.argv) != 2:
	print("usage: " + sys.argv[0] + " <directory with test-log.txt>")
	sys.exit(1)

d = sys.argv[1]
readTestLog(os.path.join(d, "test-log.txt"))

for project in projects:
	readProjectLog(project, os.path.join(d, project + ".txt"))

writeSizeMatrix(os.path.join(d, "matrix-projectsize.csv"))
