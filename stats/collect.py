import sys
import os
from collections import OrderedDict

class AnalysisInfo:
	seconds = 0.0
	reportcount = 0

class ProjectInfo:
	seconds = 0.0
	analyses = None
	def __init__(self):
		self.analyses = OrderedDict()

projects = OrderedDict()

def readTestLog(testlog):
	print("reading test-log.txt: " + testlog)
	with open(testlog) as f:
		while True:
			line = f.readline()
			if not line:
				break

			if line == "----------------------------------------\n":
				# Date
				line = f.readline()

				# (1/110) Testing on <projectname>...
				line = f.readline().rstrip("\n")
				assert "Testing on " in line
				projectname = line[line.find("Testing on ") + 11:len(line)-3]

				# ok or crashed/aborted
				line = f.readline().rstrip("\n")
				assert line == "ok" or line == "crashed/aborted"

				# Empty line
				line = f.readline().rstrip("\n")
				assert len(line) == 0

				# real\t0m5.492s
				line = f.readline().rstrip("\n")
				assert line.startswith("real\t")
				realtime = line[5:]

				# <minutes>m<seconds>s
				mpos = line.find("m")
				spos = line.find("s")
				minutes = line[5:mpos]
				seconds = line[mpos+1:spos]
				#print(realtime + ", " + minutes + ", " + seconds)

				totalseconds = "%0.3f" % (float(minutes) * 60 + float(seconds))
				#print(realtime + ", " + totalseconds)
				projects[projectname] = ProjectInfo()
				projects[projectname].seconds = totalseconds

def readProjectLog(project, projectlog):
	print("reading " + projectlog)
	with open(projectlog) as f:
		inside_analysis_count_section = False
		while True:
			line = f.readline()
			if not line:
				break
			line = line.rstrip("\n")
			if inside_analysis_count_section:
				fields = line.split()
				projects[project].analyses[fields[0]].reportcount = fields[1]
			elif line.startswith("[") and "finished" in line:
				line = line[line.find("finished")+9:]
				seconds = line[0:line.find("seconds")-1]
				analysis = line[line.find("seconds")+7:line.find(",")].strip()
				projects[project].analyses[analysis] = AnalysisInfo()
				projects[project].analyses[analysis].seconds = seconds
			elif line == "Number of reports per analysis:":
				inside_analysis_count_section = True

def writeProjectTimes(csvfile):
	print("writing " + csvfile)
	with open(csvfile, "w") as f:
		for project in projects:
			f.write(project + "," + projects[project].seconds + "\n")

def analysisIsSignificant(analysis):
	maxpercentage = 0
	for project in projects:
		maxseconds = 0.0
		if analysis in projects[project].analyses:
			maxseconds = max(maxseconds, float(projects[project].analyses[analysis].seconds))
		percentage = (100 * maxseconds) / float(projects[project].seconds)
		maxpercentage = max(maxpercentage, percentage)
	issignificant = maxpercentage > 10
	if not issignificant:
		print("insignificant analysis (only " + ("%.1f" % maxpercentage) + "% max" + "): " + analysis)

	return issignificant

def writeTimeMatrix(csvfile, usepercent, significantonly):
	print("writing time matrix: " + csvfile)
	with open(csvfile, "w") as f:
		allanalyses = list(projects["ant"].analyses.keys())
		significantanalyses = []

		if significantonly:
			# Ignore insignificant analyses
			for analysis in allanalyses:
				if analysisIsSignificant(analysis):
					significantanalyses.append(analysis)
		else:
			significantanalyses = allanalyses

		for analysis in significantanalyses:
			f.write("," + analysis)
		f.write("\n")

		for project in projects:
			f.write(project)
			for analysis in significantanalyses:
				f.write(",")
				analyses = projects[project].analyses
				totalseconds = float(projects[project].seconds)
				if analysis in analyses:
					seconds = analyses[analysis].seconds
					if usepercent:
						percent = (100 * float(seconds)) / totalseconds
						f.write("%.1f" % percent)
					else:
						f.write(seconds)
				else:
					f.write("unknown")
			f.write("\n")

def writeReportCounts(csvfile):
	print("writing report count matrix: " + csvfile)
	with open(csvfile, "w") as f:
		allanalyses = list(projects["ant"].analyses.keys())

		for analysis in allanalyses:
			f.write("," + analysis)
		f.write("\n")

		for project in projects:
			f.write(project)
			for analysis in allanalyses:
				f.write(",")
				analyses = projects[project].analyses
				if analysis in analyses:
					f.write(str(analyses[analysis].reportcount))
				else:
					f.write("unknown")
			f.write("\n")

if len(sys.argv) != 2:
	print("usage: " + sys.argv[0] + " <directory with test-log.txt>")
	sys.exit(1)

d = sys.argv[1]
readTestLog(os.path.join(d, "test-log.txt"))
writeProjectTimes(os.path.join(d, "projecttimes.csv"))

for project in projects:
	readProjectLog(project, os.path.join(d, project + ".txt"))
	projects[project].analyses = OrderedDict(sorted(projects[project].analyses.items()))

writeTimeMatrix(os.path.join(d, "matrix-time.csv"), False, False)
writeTimeMatrix(os.path.join(d, "matrix-time-percent.csv"), True, False)
writeTimeMatrix(os.path.join(d, "matrix-time-percent-significant.csv"), True, True)
writeReportCounts(os.path.join(d, "matrix-reportcounts.csv"))
